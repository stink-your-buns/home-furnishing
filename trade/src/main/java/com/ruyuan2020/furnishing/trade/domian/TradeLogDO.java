package com.ruyuan2020.furnishing.trade.domian;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@TableName("ry_trade_log")
public class TradeLogDO extends BaseDO {

    /**
     * 交易流水号
     */
    private String tradeNo;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 支付类型
     * 01.充值
     * 02.支付托管金
     */
    private String paymentType;

    /**
     * 支付方式
     */
    private String paymentMethod;

    /**
     * 金币
     */
    private BigDecimal gold;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 交易状态
     */
    private String status;

    /**
     * 招标id
     * 支付招标保证金时使用
     */
    private Long tenderId;
}
