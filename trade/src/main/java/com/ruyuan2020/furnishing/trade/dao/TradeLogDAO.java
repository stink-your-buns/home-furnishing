package com.ruyuan2020.furnishing.trade.dao;

import com.ruyuan2020.common.domain.BasePage;
import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.trade.domian.TradeLogDO;
import com.ruyuan2020.furnishing.trade.domian.TradeQuery;

public interface TradeLogDAO extends BaseDAO<TradeLogDO> {

    TradeLogDO getByTradeNo(String tradeNo);

    void updateStatus(Long id, String status);

    BasePage<TradeLogDO> listPage(TradeQuery tradeQuery);
}
