package com.ruyuan2020.furnishing.trade.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.trade.domian.TradeLogDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TradeLogMapper extends BaseMapper<TradeLogDO> {
}
