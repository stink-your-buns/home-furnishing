package com.ruyuan2020.furnishing;

import com.ruyuan2020.furnishing.common.config.FastJsonConfig;
import com.ruyuan2020.furnishing.common.config.GlobalExceptionConfig;
import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({FastJsonConfig.class, GlobalExceptionConfig.class})
@EnableDubbo
public class ThirdPayMockApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirdPayMockApplication.class, args);
    }
}
