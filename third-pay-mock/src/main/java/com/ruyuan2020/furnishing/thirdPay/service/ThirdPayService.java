package com.ruyuan2020.furnishing.thirdPay.service;

import com.ruyuan2020.furnishing.thirdPay.domain.PayRequest;

public interface ThirdPayService {

    String sdkExecute(PayRequest payRequest);
}
