package com.ruyuan2020.furnishing.thirdPay.api;

import com.ruyuan2020.furnishing.thirdPay.domain.PayRequest;
import com.ruyuan2020.furnishing.thirdPay.service.ThirdPayService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService(version = "1.0.0", interfaceClass = ThirdPayApi.class)
public class ThirdPayApiImpl implements ThirdPayApi {

    @Autowired
    private ThirdPayService thirdPayService;

    @Override
    public String sdkExecute(PayRequest payRequest) {
        return thirdPayService.sdkExecute(payRequest);
    }
}
