package com.ruyuan2020.furnishing.payment.constant;

public class PaymentConstants {

    public static final Integer PAYMENT_STATUS_WAIT_BUYER_PAY = 0;

    public static final Integer PAYMENT_STATUS_SUCCESS = 2;
}
