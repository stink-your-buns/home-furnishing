package com.ruyuan2020.furnishing.payment.controller;

import com.alibaba.fastjson.JSONObject;
import com.ruyuan2020.common.domain.JsonResult;
import com.ruyuan2020.common.util.ResultHelper;
import com.ruyuan2020.furnishing.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    /**
     * 支付回调
     *
     * @param body 请求body
     */
    @PostMapping("/notify")
    public JsonResult<?> notify(@RequestBody JSONObject body) {
        String resultCode = body.getString("result_code");
        String tradeNo = body.getString("out_trade_no");
        return ResultHelper.ok(paymentService.notify(resultCode, tradeNo));
    }

}
