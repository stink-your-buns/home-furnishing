package com.ruyuan2020.furnishing.account.constant;

import java.math.BigDecimal;

public class PointConstants {

    public static final BigDecimal POINT_RATE = BigDecimal.valueOf(5);
}
