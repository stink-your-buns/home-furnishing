package com.ruyuan2020.furnishing.account.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDTO extends BaseDomain {

    private Long memberId;
}
