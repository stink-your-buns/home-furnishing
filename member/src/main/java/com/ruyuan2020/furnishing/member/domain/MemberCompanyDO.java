package com.ruyuan2020.furnishing.member.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName("ry_member_company")
public class MemberCompanyDO extends BaseDO {

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 投标数量
     */
    private Integer bidQuantity;

    /**
     * 投标签约数
     */
    private Integer SignedQuantity;

    /**
     * 评价数
     */
    private Integer commentQuantity;

}
